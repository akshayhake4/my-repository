package com.example.mytimer;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.app.NotificationChannel;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity  {

    private static final String CHANNEL_ID = "channel1";
    TextView texthr,textmin,textsec,textrecieve;
    int min=0,sec=0,ms=0,hr=0;
    String lt;
    public static final String mypackage="com.example.mytimer";
    public static final String stime="Time";
    public static final String hr1="hr";
    public static final String min1="min";
    public static final String sec1="sec";
    long diff,days=0,ch=0,cm=0,cs=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel =new NotificationChannel(CHANNEL_ID,CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Notification");
            NotificationManager notificationManager=getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        texthr=findViewById(R.id.texthrs);
        textmin=findViewById(R.id.textmin);
        textsec=findViewById(R.id.textsecond);




        long lasttime=0;

        Date curDate=new Date();
        SharedPreferences sp=getSharedPreferences(mypackage, Context.MODE_PRIVATE);
        lasttime=sp.getLong("lasttime",new Date().getTime());
        diff = curDate.getTime()-lasttime;
        days = diff / (24 * 60 * 60 * 1000);
        diff -= days * (24 * 60 * 60 * 1000);
        ch = diff / (60 * 60 * 1000);
        diff -= ch * (60 * 60 * 1000);
        cm = diff / (60 * 1000);
        diff -= cm * (60 * 1000);
        cs = diff / 1000;

        lt=""+cs+" Sec";


        if(cm!=0) lt=""+cm + " Min and "+cs+" Sec";
        if(ch!=0) lt=""+ch +" Hr, "+ cm + " Min and "+cs+" Sec";
        if(days!=0) lt=""+ days+" Days, "+ch +" Hr, "+ cm + " Min and "+cs+" Sec";






        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.time)
                .setContentTitle("New notification")
                .setContentText("App was closed for  " + lt )
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat;
        notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(1,builder.build());


        Timer timer=new Timer();
       timer.schedule(new TimerTask() {
           @Override
           public void run() {



               while(true)
               {


                   sec+=1;
                   if(sec==60) {
                       sec=0;
                       min=min+1;
                   }
                   if(min==60) {
                       min=0;
                       hr+=1;
                   }
                   if(hr==60)
                       hr=0;



                   textsec.setText(String.valueOf(sec));
                   textmin.setText(String.valueOf(min));
                   texthr.setText(String.valueOf(hr));




                   try {
                       Thread.sleep(1000);
                   } catch (InterruptedException e) {

                   }

               }

           }
       },1000);




    }


    @Override
    protected void onStop() {

        int thr, tmin, tsec;
        Date date = new Date();

        SharedPreferences sp = getSharedPreferences(mypackage, Context.MODE_PRIVATE);
        sp.edit().putLong("lasttime",date.getTime()).commit();

        super.onStop();


    }
}


